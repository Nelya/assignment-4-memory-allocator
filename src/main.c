#define _DEFAULT_SOURCE
#include "mem.h"
#include "mem_internals.h"
#include "util.h"
#include <assert.h>
#include <sys/mman.h>
#define FAIL "Test failed:("

static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, -1, 0);
}

static struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

static void print_before_state(void* heap){
    puts("Before:");
    debug_heap(stdout, heap);
}

static void print_after_state(void* heap){
    puts("After:");
    debug_heap(stdout, heap);
}

static void print_free_state(void* heap){
    puts("Free...");
    debug_heap(stdout, heap);
}

static void end_test(){
    heap_term();
    puts("\n");
}

static void test_malloc() {
    puts("Memory has been allocated successfully");
    void* heap = heap_init(0);
    print_before_state(heap);
    void *mem = _malloc(128);
    print_after_state(heap);
    assert(mem != NULL && FAIL);
    _free(mem);
    print_free_state(heap);
    end_test();
}

static void test_freeing_block() {
    puts("Freeing one block");
    void *heap = heap_init(0);
    print_before_state(heap);
    void *mem1 = _malloc(64);
    void *mem2 = _malloc(128);
    print_after_state(heap);
    assert(mem1 != NULL && FAIL);
    assert(mem2 != NULL && FAIL);
    _free(mem1);
    print_free_state(heap);
    _free(mem2);
    end_test();
}

static void test_freeing_blocks() {
    puts("Freeing two blocks");
    void *heap = heap_init(0);
    print_before_state(heap);
    void *mem1 = _malloc(64);
    void *mem2 = _malloc(128);
    void *mem3 = _malloc(256);
    void *mem4 = _malloc(512);
    print_after_state(heap);
    assert(mem1 != NULL && FAIL);
    assert(mem2 != NULL && FAIL);
    assert(mem3 != NULL && FAIL);
    assert(mem4 != NULL && FAIL);
    _free(mem1);
    _free(mem2);
    print_free_state(heap);
    _free(mem3);
    _free(mem4);
    end_test();
}

static void test_extension() {
    puts("New memory region expands the old one");
    struct region *heap = heap_init(0);
    print_before_state(heap);
    void *mem1 = _malloc(REGION_MIN_SIZE / 2);
    void *mem2 = _malloc(REGION_MIN_SIZE * 2);
    assert(mem1 != NULL && FAIL);
    assert(mem2 != NULL && FAIL);
    assert(mem2 - REGION_MIN_SIZE / 2 - offsetof(struct block_header, contents) == mem1 && FAIL);
    print_after_state(heap);
    _free(mem1);
    _free(mem2);
    print_free_state(heap);
    end_test();
}

int main() {
    test_malloc();
    test_freeing_block();
    test_freeing_blocks();
    test_extension();
}
